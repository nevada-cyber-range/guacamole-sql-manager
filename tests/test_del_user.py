#!/usr/bin/env python3
import unittest
import logging
import db_scripts
from types import SimpleNamespace


class TestDelUser(unittest.TestCase):
    def test_normal(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = 'test1'
        cursor, guacDB = db_scripts.guac_db_connect()

        db_scripts.add_user("testuser",conn_name,logger)
        db_scripts.del_user("testuser")
        sql = "select entity_id from guacamole_entity where name = '%s'"%conn_name
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertEqual([],res)


if __name__ == '__main__':
    unittest.main()
