#!/usr/bin/env python3
import unittest

import logging
import db_scripts.guac_db_conn as gdb
import db_scripts
from types import SimpleNamespace


class TestAddRoView(unittest.TestCase):
    def test_normal(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = 'test1'
        cursor, guacDB = gdb.guac_db_connect()
        args = SimpleNamespace(conn_group='CS491',parent_conn_group=None,conn_name='test1',view_name='test1ro',netid_file="../input/example-users")

        db_scripts.add_conn(conn_name,"1234","CS491",logger)
        db_scripts.add_ro_view(args,logger)
        sql = "select connection_id from guacamole_connection where connection_name = '%s'"%conn_name
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertIsNotNone(res)
        sql = "DELETE from guacamole_connection where connection_name = '%s'"%conn_name
        cursor.execute(sql)
        guacDB.commit()

    def test_no_name(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = 'test1'
        cursor, guacDB = gdb.guac_db_connect()
        args = SimpleNamespace(conn_group='CS491',parent_conn_group=None,conn_name=None,view_name='test1ro',netid_file="../input/example-users")
        db_scripts.add_conn(conn_name,"1234","CS491",logger)
        ret=db_scripts.add_ro_view(args,logger)
        self.assertEqual("NONAME",ret)

    def test_no_group(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = 'test1'
        cursor, guacDB = gdb.guac_db_connect()
        args = SimpleNamespace(conn_group=None,parent_conn_group=None,conn_name="test",view_name='test1ro',netid_file="../input/example-users")
        db_scripts.add_conn(conn_name,"1234","CS491",logger)
        ret=db_scripts.add_ro_view(args,logger)
        self.assertEqual("NOGROUP",ret)

    def test_no_view(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = 'test1'
        cursor, guacDB = gdb.guac_db_connect()
        args = SimpleNamespace(conn_group="CS491",parent_conn_group=None,conn_name="test",view_name=None,netid_file="../input/example-users")
        db_scripts.add_conn(conn_name,"1234","CS491",logger)
        ret=db_scripts.add_ro_view(args,logger)
        self.assertEqual("NOVIEWNAME",ret)

    def test_no_netid(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = 'test1'
        cursor, guacDB = gdb.guac_db_connect()
        args = SimpleNamespace(conn_group="CS491",parent_conn_group=None,conn_name="test",view_name="view",netid_file=None)
        db_scripts.add_conn(conn_name,"1234","CS491",logger)
        ret=db_scripts.add_ro_view(args,logger)
        self.assertEqual("NONETID",ret)

if __name__ == '__main__':
    unittest.main()
