#!/usr/bin/env python3
import unittest

import logging
from types import SimpleNamespace
import db_scripts.guac_db_conn as gdb
import db_scripts

class TestAddPerm(unittest.TestCase):
    def test_normal(self):
        logger = logging.getLogger('ncr-sql')
        group_name = 'test'
        cursor, guacDB = gdb.guac_db_connect()
        args = SimpleNamespace(conn_group='test',parent_conn_group=None)

        db_scripts.add_conn("test","1234","CS491",logger)
        db_scripts.add_perm("test","ian","CS491",logger)
        sql = "select entity_id from guacamole_entity where name = '%s'" % "ian"
        cursor.execute(sql)
        res = cursor.fetchall()
        entity_id = res[0][0]
        sql = "select connection_id from guacamole_connection where connection_name = '%s'" % "test"
        cursor.execute(sql)
        res = cursor.fetchall()
        conn_id = res[0][0]
        sql = "select * from guacamole_connection_permission where connection_id = '%s' and entity_id ='%s'" % (conn_id,entity_id)
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertIsNotNone(res)
        sql = "DELETE from guacamole_connection_group where connection_group_name = '%s'" % group_name
        cursor.execute(sql)
        guacDB.commit()

    def test_normal(self):
        logger = logging.getLogger('ncr-sql')
        group_name = 'test'
        cursor, guacDB = gdb.guac_db_connect()
        args = SimpleNamespace(conn_group='test',parent_conn_group=None)

        db_scripts.add_conn("test","1234","CS491",logger)
        db_scripts.add_perm("test","ian","CS491",logger)
        sql = "select connection_group_id from guacamole_connection_group where connection_group_name = '%s'" % group_name
        sql = "select entity_id from guacamole_entity where name = '%s'" % "ian"
        cursor.execute(sql)
        res = cursor.fetchall()
        entity_id = res[0][0]
        sql = "select connection_id from guacamole_connection where connection_name = '%s'" % "test"
        cursor.execute(sql)
        res = cursor.fetchall()
        conn_id = res[0][0]
        sql = "select * from guacamole_connection_permission where connection_id = '%s' and entity_id ='%s'" % (conn_id,entity_id)
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertIsNotNone(res)
        sql = "DELETE from guacamole_connection_group where connection_group_name = '%s'" % group_name
        cursor.execute(sql)
        guacDB.commit()

    def test_missing_user_group(self):
        logger = logging.getLogger('ncr-sql')
        group_name = 'test'
        cursor, guacDB = gdb.guac_db_connect()
        args = SimpleNamespace(conn_group='test',parent_conn_group=None)

        db_scripts.add_conn("test","1234","CS491",logger)
        db_scripts.add_perm("test","ian","",logger)
        sql = "select connection_group_id from guacamole_connection_group where connection_group_name = '%s'" % group_name
        sql = "select entity_id from guacamole_entity where name = '%s'" % "ian"
        cursor.execute(sql)
        res = cursor.fetchall()
        entity_id = res[0][0]
        sql = "select connection_id from guacamole_connection where connection_name = '%s'" % "test"
        cursor.execute(sql)
        res = cursor.fetchall()
        conn_id = res[0][0]
        sql = "select * from guacamole_connection_permission where connection_id = '%s' and entity_id ='%s'" % (conn_id,entity_id)
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertIsNotNone(res)
        sql = "DELETE from guacamole_connection where connection_name = 'test'"
        cursor.execute(sql)
        guacDB.commit()

if __name__ == '__main__':
    unittest.main()
