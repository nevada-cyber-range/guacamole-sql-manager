#!/usr/bin/env python3
import unittest

import logging
import db_scripts.guac_db_conn as gdb
import db_scripts.add_conn as ac
import db_scripts.del_conn as dc


class TestDelConn(unittest.TestCase):
    def test_normal(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = 'test1'
        cursor, guacDB = gdb.guac_db_connect()

        ac(conn_name, "1234", "CS491", logger)
        dc.del_conn(conn_name,logger)
        sql = "select connection_id from guacamole_connection where connection_name = '%s'" % conn_name
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertListEqual(res,[])
        guacDB.commit()

    def test_not_exist(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = 'test1'
        cursor, guacDB = gdb.guac_db_connect()

        dc.del_conn(conn_name,logger)
        sql = "select connection_id from guacamole_connection where connection_name = '%s'" % conn_name
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertListEqual(res,[])
        guacDB.commit()


if __name__ == '__main__':
    unittest.main()
