#!/usr/bin/env python3
import unittest

import logging
import db_scripts.guac_db_conn as gdb
import db_scripts
from types import SimpleNamespace


class TestAddUser2Group(unittest.TestCase):
    def test_normal(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = 'test1'
        cursor, guacDB = gdb.guac_db_connect()

        db_scripts.add_user("testuser",conn_name,logger)
        db_scripts.add_user_to_group("testuser","CS491",logger)
        sql = "select permission from guacamole_connection_group_permission where connection_group_id='16'"
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertIsNotNone(res)
        sql = "DELETE from guacamole_entity where name = 'testuser'"
        cursor.execute(sql)
        guacDB.commit()


if __name__ == '__main__':
    unittest.main()
