#!/usr/bin/env python3
import unittest

import logging
from types import SimpleNamespace
import db_scripts.guac_db_conn as gdb
import db_scripts.add_group as ag


class TestAddGroup(unittest.TestCase):
    def test_normal(self):
        logger = logging.getLogger('ncr-sql')
        group_name = 'test'
        cursor, guacDB = gdb.guac_db_connect()
        args = SimpleNamespace(conn_group='test',parent_conn_group=None)

        ag(args,logger)
        sql = "select connection_group_id from guacamole_connection_group where connection_group_name = '%s'" % group_name
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertIsNotNone(res)
        sql = "DELETE from guacamole_connection_group where connection_group_name = '%s'" % group_name
        cursor.execute(sql)
        guacDB.commit()

    def test_parent(self):
        logger = logging.getLogger('ncr-sql')
        group_name = 'test1'
        cursor, guacDB = gdb.guac_db_connect()
        args = SimpleNamespace(conn_group='test',parent_conn_group=None)
        args2 = SimpleNamespace(conn_group='test1',parent_conn_group='test')

        ag(args,logger)
        ag(args2,logger)
        sql = "select connection_group_id from guacamole_connection_group where connection_group_name = '%s'" % group_name
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertIsNotNone(res)
        sql = "DELETE from guacamole_connection_group where connection_group_name = '%s'" % group_name
        cursor.execute(sql)
        guacDB.commit()

    def test_parent_missing(self):
        logger = logging.getLogger('ncr-sql')
        group_name = 'test1'
        cursor, guacDB = gdb.guac_db_connect()
        args = SimpleNamespace(conn_group='test',parent_conn_group=None)
        args2 = SimpleNamespace(conn_group='test1',parent_conn_group='Going to Get Milk')

        ag(args,logger)
        ag(args2,logger)
        sql = "select connection_group_id from guacamole_connection_group where connection_group_name = '%s'" % group_name
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertEqual(res,[])
        sql = "DELETE from guacamole_connection_group where connection_group_name = '%s'" % group_name
        cursor.execute(sql)
        guacDB.commit()

if __name__ == '__main__':
    unittest.main()
