#!/usr/bin/env python3

import unittest

import logging
import db_scripts.guac_db_conn as gdb
import db_scripts.add_conn as ac


class TestGuacDB(unittest.TestCase):
    def test_DB(self):
        logger = logging.getLogger('ncr-sql')
        cursor, guacDB = gdb.guac_db_connect()

        sql = "show tables;"
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertIsNotNone(res)


if __name__ == '__main__':
    unittest.main()
