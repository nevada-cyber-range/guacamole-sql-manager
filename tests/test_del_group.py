#!/usr/bin/env python3
import unittest
import logging
import db_scripts
from types import SimpleNamespace


class TestDelGroup(unittest.TestCase):
    def test_normal(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = 'test1'
        cursor, guacDB = db_scripts.guac_db_connect()
        args = SimpleNamespace(conn_group="test",parent_conn_group=None)

        db_scripts.add_group(args,logger)
        db_scripts.del_group(args,logger)
        sql = "select connection_group_id from guacamole_connection_group where connection_group_name = 'test'"
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertEqual([],res)


if __name__ == '__main__':
    unittest.main()
