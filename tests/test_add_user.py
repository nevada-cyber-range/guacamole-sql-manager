#!/usr/bin/env python3
import unittest

import logging
import db_scripts.guac_db_conn as gdb
import db_scripts
from types import SimpleNamespace


class TestAddUser(unittest.TestCase):
    def test_normal(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = 'test1'
        cursor, guacDB = gdb.guac_db_connect()
        args = SimpleNamespace(conn_group='CS491',parent_conn_group=None,conn_name='test1',view_name='test1ro',netid_file="../input/example-users")

        db_scripts.add_user("testuser",conn_name,logger)
        sql = "select entity_id from guacamole_entity where name = '%s'"%conn_name
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertIsNotNone(res)
        sql = "DELETE from guacamole_entity where name = 'testuser'"
        cursor.execute(sql)
        guacDB.commit()

    def test_dup(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = 'test1'
        cursor, guacDB = gdb.guac_db_connect()
        args = SimpleNamespace(conn_group='CS491',parent_conn_group=None,conn_name='test1',view_name='test1ro',netid_file="../input/example-users")

        db_scripts.add_user("testuser",conn_name,logger)
        ret=db_scripts.add_user("testuser",conn_name,logger)
        self.assertEqual("DUP",ret)
        sql = "DELETE from guacamole_entity where name = 'testuser'"
        cursor.execute(sql)
        guacDB.commit()


if __name__ == '__main__':
    unittest.main()
