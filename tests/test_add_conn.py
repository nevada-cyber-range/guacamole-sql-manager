#!/usr/bin/env python3
import unittest

import logging
import db_scripts.guac_db_conn as gdb
import db_scripts.add_conn as ac


class TestAddConn(unittest.TestCase):
    def test_normal(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = 'test1'
        cursor, guacDB = gdb.guac_db_connect()

        ac(conn_name,"1234","CS491",logger)
        sql = "select connection_id from guacamole_connection where connection_name = '%s'"%conn_name
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertIsNotNone(res)
        sql = "DELETE from guacamole_connection where connection_name = '%s'"%conn_name
        cursor.execute(sql)
        guacDB.commit()

    def test_duplicate(self):
        logger = logging.getLogger('ncr-sql')
        cursor, guacDB = gdb.guac_db_connect()

        conn_name = 'test1'
        ac(conn_name,"1234","CS491",logger)
        ret = ac(conn_name,"1234","CS491",logger)
        self.assertEqual("SQLERROR",ret)
        #cleanup
        sql = "DELETE from guacamole_connection where connection_name = '%s'"%conn_name
        cursor.execute(sql)
        guacDB.commit()

    def test_no_name(self):
        logger = logging.getLogger('ncr-sql')
        cursor, guacDB = gdb.guac_db_connect()

        conn_name = ''
        ret=ac(conn_name,"1234","CS491",logger)
        self.assertEqual("NONAME",ret)
        #cleanup
        sql = "DELETE from guacamole_connection where connection_name = '%s'"%conn_name
        cursor.execute(sql)
        guacDB.commit()

    def test_no_group(self):
        logger = logging.getLogger('ncr-sql')
        cursor, guacDB = gdb.guac_db_connect()

        conn_name = 'test1'
        ret=ac(conn_name,"1234","",logger)
        self.assertEqual("NOGROUP",ret)
        #cleanup
        sql = "DELETE from guacamole_connection where connection_name = '%s'"%conn_name
        cursor.execute(sql)
        guacDB.commit()

    def test_no_group(self):
        logger = logging.getLogger('ncr-sql')
        cursor, guacDB = gdb.guac_db_connect()

        conn_name = 'test1'
        ret=ac(conn_name,"","CS491",logger)
        self.assertEqual("NOPORT",ret)
        #cleanup
        sql = "DELETE from guacamole_connection where connection_name = '%s'"%conn_name
        cursor.execute(sql)
        guacDB.commit()


if __name__ == '__main__':
    unittest.main()
