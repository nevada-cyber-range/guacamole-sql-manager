FROM mariadb:bionic

WORKDIR /usr/src/app

env MYSQL_ROOT_PASSWORD=root

COPY . .

RUN apt update -y

RUN apt install python3 python3-pip -y

RUN pip3 install -r requirements.txt

RUN service mysql start

#RUN mysql < createTestDb.sql

EXPOSE 3306
