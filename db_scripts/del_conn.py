#!/usr/bin/env python3

import mysql.connector
import db_scripts.guac_db_conn as gdb

def del_conn(conn_name,logger):
    cursor,guacDB = gdb.guac_db_connect()

    #mycursor.execute("SHOW TABLES")
    #get conn id from conn entry
    sql = "select connection_id from guacamole_connection where connection_name = '%s'"%conn_name
    try:
        cursor.execute(sql)
        res = cursor.fetchall()
        conn_id = res[0][0]
    except:
        logger.error("Could not locate connection(%s)")
        return
    sql = "DELETE from guacamole_connection where connection_name = '%s'"%conn_name
    cursor.execute(sql)
    guacDB.commit()
    sql = "select * from guacamole_connection where connection_name = '%s'"%conn_name
    cursor.execute(sql)
    res = cursor.fetchall()
    print(res)
    sql = "DELETE from guacamole_connection_parameter where connection_id = '%s'"%conn_id
    cursor.execute(sql)
    guacDB.commit()
    guacDB.commit()

    #clean up
    cursor.close()
    guacDB.close()
