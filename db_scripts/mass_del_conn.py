#!/usr/bin/env python3

import mysql.connector
import db_scripts

def mass_del_conn(args=None,logger=None):

    choice = input("Mass remove by connection group(1) or by file(2)?\n(1/2): ")
    if choice == '1':
        if args.conn_group is None:
            logger.info("Missing conn_group argument")
            exit(1)
        logger.info("Removing by group(%s)"%args.conn_group)
        db_scripts.mass_del_conn_group(args,logger)        
    elif choice == '2':
        if args.conn_file is None:
            logger.info("Missing conn_file argument")
            exit(1)
        logger.info("Removing all connections listed in %s"%args.conn_file)
        db_scripts.mass_del_conn_file(args,logger)
    else:
        logger.info("User didn't choose 1 or 2 so exiting")
        exit(1)
