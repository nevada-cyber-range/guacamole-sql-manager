#!/usr/bin/env python3

import mysql.connector
import db_scripts.guac_db_conn as gdb


""""
+---------------------+-----------+-----------------------+----------------+-----------------+--------------------------+-------------------------+
| connection_group_id | parent_id | connection_group_name | type           | max_connections | max_connections_per_user | enable_session_affinity |
+---------------------+-----------+-----------------------+----------------+-----------------+--------------------------+-------------------------+
|                   1 |      NULL | Kali                  | BALANCING      |            NULL |                     NULL |                       0 |
|                   4 |      NULL | cs447                 | ORGANIZATIONAL |            NULL |                     NULL |                       0 |
+---------------------+-----------+-----------------------+----------------+-----------------+--------------------------+-------------------------+
"""

def add_group(args=None,logger=None):

    cursor,guacDB = gdb.guac_db_connect()

    cursor = guacDB.cursor()
    parentID = None

    if args.conn_group is None:
        logger.error("conn_group missing for ",args.conn_group)
        return

    #get conn id from conn entry
    sql = "select connection_group_id from guacamole_connection_group where connection_group_name = '%s'"%args.conn_group
    try:
        cursor.execute(sql)
        res = cursor.fetchall()
        conn_id = res[0][0]
        logger.warning("Group(%s) Already Exists"%args.conn_group)
        return
    except:
        logger.info("Group(%s) is free"%args.conn_group)

    if args.parent_conn_group is not None:
        #get conn id from conn entry
        sql = "select connection_group_id from guacamole_connection_group where connection_group_name = '%s'"%args.parent_conn_group
        try:
            cursor.execute(sql)
            res = cursor.fetchall()
            conn_id = res[0][0]
            logger.info("Parent Group(%s(%s)) found"%(args.conn_group,conn_id))
            parentID = conn_id
        except:
            logger.warning("Parent Group(%s) Missing"%args.conn_group)
            return

    logger.info("Creating Guacamole Connection Group: %s"%args.conn_group)

    #sql = "INSERT INTO guacamole_connection_group (connection_group_name, type,max_connections_per_user) VALUES (%s,%s,%s)"
    #val = (args.conn_group, 'ORGANIZATIONAL','3') #3 conns per user incase they have a session open at home or something
    #if parentID is not None:
    sql = "INSERT INTO guacamole_connection_group (connection_group_name, parent_id,type,max_connections_per_user) VALUES (%s,%s,%s,%s)"
    val = (args.conn_group, parentID ,'ORGANIZATIONAL','3') #3 conns per user incase they have a session open at home or something
    try:
        cursor.execute(sql, val)
        guacDB.commit()
        logger.info("Connection Group(%s) created"%args.conn_group)
    except:
        logger.error("Connection Group(%s) failed to be created"%args.conn_group)
        return
