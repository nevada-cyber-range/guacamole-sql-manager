#!/usr/bin/env python3

import mysql.connector
import db_scripts.guac_db_conn as gdb
import db_scripts

def add_ro_view(args=None,logger=None):

    if args.conn_group is None:
        logger.error("Missing conn_group arg")
        return "NOGROUP"
    if args.conn_name is None:
        logger.error("Missing conn_name arg")
        return "NONAME"
    if args.view_name is None:
        logger.error("Missing view_name arg")
        return "NOVIEWNAME"
    if args.netid_file is None:
        logger.error("Missing netid_file arg")
        return "NONETID"
    cursor,guacDB = gdb.guac_db_connect()

    Parent_Id = None
    hostport = None
    netids = []
    Protocol = 'vnc'
    if args.conn_name is not None:
        #get conn id to get hostport
        sql = "select connection_id from guacamole_connection where connection_name = '%s'"%args.conn_name
        try:
            cursor.execute(sql)
            conn_id = cursor.fetchall()[0][0]
            logger.info("Found conn id (%s) for connection (%s)"%(conn_id,args.conn_name))
        except:
            logger.error("Could not find conn id for connection (%s) in connection list"%args.conn_name)
            exit(1)
        #get hostport
        sql = "select parameter_value from guacamole_connection_parameter where connection_id = '%s' and parameter_name = 'port'"%conn_id
        cursor.execute(sql)
        hostport = cursor.fetchall()[0][0]
        logger.info("Found hostport(%s) for connection: %s"%(hostport,args.conn_name))

    db_scripts.add_conn(args.view_name,hostport,args.conn_group,logger,read_only='true',max_conns='50')

    try:
        netid_file = open(args.netid_file)
    except:
        logger.error("Error reading connection file")
        return -1

    for line in netid_file:
        netids.append(line.strip('\n').split(' '))

    netid_file.close()
    for netidLine in netids:
        netid = netidLine[0]
        db_scripts.add_perm(args.view_name,netid,args.conn_group,logger)

    #clean up
    cursor.close()
    guacDB.close()
    logger.info("Cleaned up and exited properly")
