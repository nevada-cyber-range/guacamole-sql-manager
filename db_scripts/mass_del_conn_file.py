#!/usr/bin/env python3

import mysql.connector
import db_scripts.guac_db_conn as gdb

def mass_del_conn_file(args=None,logger=None):
    cursor,guacDB = gdb.guac_db_connect()

    #choice = input("Are you sure you want to delete all connections in file(%s)?\n(y/n): "%args.conn_file)
    #if choice.lower() != 'y':
    #    logger.info("Aborting deletion of connections in file(%s)"%args.conn_file)
    #    exit(0)

    conns = []
    conn_file = open(args.conn_file)
    for line in conn_file:
        conns.append(line.strip('\n'))
    for conn in conns:
        conn_name = conn.split(' ')[0]
        print(conn_name)
        #get conn id from conn entry
        sql = "select connection_id from guacamole_connection where connection_name = '%s'"%conn_name
        cursor.execute(sql)
        res = cursor.fetchall()
        print(res)
        conn_id = res[0][0]
        logger.info("Deleting Connection(%s) Id(%s)"%(conn_name,conn_id))
        sql = "DELETE from guacamole_connection where connection_name = '%s'"%conn_name
        cursor.execute(sql)
        guacDB.commit()

    #clean up
    conn_file.close()
    cursor.close()
    guacDB.close()

