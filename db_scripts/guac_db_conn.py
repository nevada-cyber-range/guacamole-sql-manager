#!/usr/bin/env python3

import mysql.connector
import warnings

warnings.filterwarnings(action="ignore", message="unclosed",category=ResourceWarning)

def guac_db_connect():
    guacDB = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="guacamole_db_1"
    )
    cursor = guacDB.cursor()
    return (cursor,guacDB)
