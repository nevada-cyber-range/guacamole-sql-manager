#!/usr/bin/env python3

import mysql.connector
import db_scripts.guac_db_conn as gdb

def del_user(netid):
    cursor,guacDB = gdb.guac_db_connect()

    #get entity id
    sql = "select entity_id from guacamole_entity where name = '%s'"%netid
    cursor.execute(sql)
    res = cursor.fetchall()
    if len(res) == 0:
        print("User %s not found"%netid)
        exit(1)
    entity_id = res[0][0]
    #Remove Entity
    sql = "DELETE from guacamole_entity where name = '%s'"%netid
    cursor.execute(sql)
    guacDB.commit()

    #close conns
    cursor.close()
    guacDB.close()
