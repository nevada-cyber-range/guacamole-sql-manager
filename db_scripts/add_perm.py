import mysql.connector
import db_scripts.guac_db_conn as gdb
import db_scripts

def add_perm(conn_name,netid,conn_group,logger):
    cursor,guacDB = gdb.guac_db_connect()

    cursor = guacDB.cursor()

    if len(conn_name) == 0:
        logger.info("Conn_name missing for %s"%netid)
        return
    if len(netid) == 0:
        logger.info("netid missing for %s"%conn_name)
        return
    #GET entity_ID
    sql = "select entity_id from guacamole_entity where name = '%s'"%netid
    try:
        cursor.execute(sql)
        res = cursor.fetchall()
        entity_id = res[0][0]
    except:
        logger.error("Failed to get entity id for user(%s) when trying to add_perm for connection(%s)"%(netid,conn_name))
        return
    #Get conn id
    sql = "select connection_id from guacamole_connection where connection_name = '%s'"%conn_name
    try:
        cursor.execute(sql)
        res = cursor.fetchall()
    except:
        logger.error("Failed to get conn id for connection(%s) when trying to add_perm for user(%s) to connection(%s)"%(conn_name,netid,conn_name))
        return
    #add user to conn
    if len(res) != 0:
        conn_id = res[0][0]
        sql = "INSERT INTO guacamole_connection_permission (entity_id,connection_id,permission) VALUES (%s, %s, %s)"
        val = (entity_id, conn_id,'READ')
        try:
            cursor.execute(sql, val)
        except:
            logger.warning("User(%s) all ready has access to connection(%s)"%(netid,conn_name))
        guacDB.commit()
    else:
        logger.error("Missing connection (%s)"%conn_name)
        exit(1)
    logger.info("User(%s) given access to Connection(%s)"%(netid,conn_name))
    #add uset to conn_group
    if conn_group is not None:
        sql = "select connection_group_id from guacamole_connection_group where connection_group_name = '%s'"%conn_group
        try:
            cursor.execute(sql)
            res = cursor.fetchall()
            group_id = res[0][0]
        except:
            logger.error("Could not locate group(%s).\nPlease check spelling of conn_group")
            return -1
        #give access to group
        sql = "INSERT INTO guacamole_connection_group_permission (entity_id,connection_group_id,permission) VALUES (%s, %s, %s)"
        val = (entity_id, group_id,'READ')
        try:
            cursor.execute(sql, val)
            guacDB.commit()
            logger.info("User(%s) given access to Group(%s)"%(netid,conn_group))
        except:
            logger.warning("User(%s) all ready has access to Group(%s)"%(netid,conn_group))
    else:
        logger.error("User(%s) not given access a Group since conn_group missing")

