#!/usr/bin/env python3

import mysql.connector
import db_scripts.guac_db_conn as gdb
import db_scripts
import datetime

def mass_add_user(args,logger):
    cursor,guacDB = gdb.guac_db_connect()

    if args.netid_file is None:
        logger.error("Missing netid_file argument")
        exit(1)

    if args.conn_group is None:
        logger.error("Missing conn_group argument")
        exit(1)

    netids = []
    try:
        netid_file = open(args.netid_file)
    except:
        logger.error("Error reading netid file")
        exit(1)
    for line in netid_file:
        netids.append(line.strip('\n').split(' '))
    
    netid_file.close()
    logger.info("Done reading netid file")
    for netidLine in netids:
        netid = netidLine[0]
        db_scripts.add_user(netid,"%s-%s"%(args.conn_group,netid),logger)
        db_scripts.add_perm("%s-%s"%(args.conn_group,netid),netid,args.conn_group,logger)
    cursor.close()
    guacDB.close()
