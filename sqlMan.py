#!/usr/bin/env python3

import os
import datetime
import db_scripts
import argparse
import logging
import subprocess
from inspect import getmembers, isfunction

parser = argparse.ArgumentParser(description='db scripts to configure and modify guac conns used in Nevada Cyber Range')
parser.add_argument('--task', type=str,required=True,
                                    help='specifies which script to run')
parser.add_argument('--conn_name',type=str,
                                    help='guac conn to be modified or deleted')
parser.add_argument('--view_name',type=str,
                                    help='guac readonly conn to be added')
parser.add_argument('--conn_group',type=str,
                                    help='guac conn group to be modified or deleted')
parser.add_argument('--parent_conn_group',type=str,
                                    help='parent guac conn group to conn_group')
parser.add_argument('--vmhost',type=str,default='ncr-0.ncr',
                                    help='ganeti host that the vm lives on')
parser.add_argument('--netid',type=str,
                                    help='single netid used for add_user\n conn associated with user needs to be $conn_group-$netid')
parser.add_argument('--netid_file',type=str,
                                    help='file of netids to be used in mass_add_user and connections user owns must be in the form of $conn_group-$netid')
parser.add_argument('--perm_file',type=str,
                                    help='file of netids to be used in mass_add_perm and connections user owns must be in the form of conn user')
parser.add_argument('--conn_file',type=str,
                                    help='space deliminated file containing conn_names and VNC ports to be used in mass_add_conn\nconnections need to be the form of $conn_group-$netid')

#LOGGING
logDir = os.path.join(os.getcwd(), 'logs')
#Make the directory
try:
    os.makedirs(logDir)
except:
    pass

logPath = os.path.join(logDir,"{0}.log".format(datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')))

#Setup logging
class LogLevelFilter(object):
    def __init__(self, level):
        self.__level = level

    def filter(self, logRecord):
        return logRecord.levelno <= self.__level

fileHandler = logging.FileHandler(logPath)
streamHandler = logging.StreamHandler()

logging.basicConfig(
    format="%(asctime)s %(funcName)s %(levelname)s  %(message)s",
    datefmt='%Y-%m-%d %H:%M:%S',
    level=logging.INFO,
    handlers=[
        fileHandler,
        streamHandler,
    ])


def main():
    logger = logging.getLogger('ncr-sql')

    args = parser.parse_args()
    functions_list = [o for o in getmembers(db_scripts) if isfunction(o[1])]
    func = None
    for f in functions_list:
        if f[0] == args.task:
            logger.info("Found function %s" % args.task)
            func = f[1]

    if func is None:
        logger.info("Function %s not found" % args.task)
        exit(1)

    func(args,logger)

if __name__ == '__main__':
    main()
